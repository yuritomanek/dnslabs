class Account < ActiveRecord::Base

  belongs_to :user

  def create_customer(token=nil)
    begin
      account = self
      current_user = account.user

      Stripe.api_key = SECRET_KEY

      customer = Stripe::Customer.create(email: current_user.email, source: token)

      account.customer_id = customer.id
      account.save
      return true
    rescue
      return false
    end
  end

  def create_subscription(quantity=1)
    begin
      account = self
      current_user = account.user

      Stripe.api_key = SECRET_KEY

      customer = Stripe::Customer.retrieve(account.customer_id)

      subscription = customer.subscriptions.create(:plan => "domain", :quantity => quantity)

      account.subscription_id = subscription.id
      account.save

      return true
    rescue
      return false
    end
  end

  def update_subscription(quantity)
    begin
      account = self
      current_user = account.user

      Stripe.api_key = SECRET_KEY

      customer = Stripe::Customer.retrieve(account.customer_id)

      subscription = customer.subscriptions.retrieve(account.subscription_id)
      subscription.quantity = quantity
      subscription.save

      return true
    rescue
      return false
    end
  end

end
