class Forwarder < ActiveRecord::Base

  before_create :check_http
  after_create :setup_a_record
  before_destroy :destroy_a_record

  belongs_to :domain
  has_one :a_record, class_name: 'A'

  def check_http
    if !self.content.include?("http://") || !self.content.include?("https://")
      content = self.content
      self.content = content.prepend("http://")
    end
  end

  def setup_a_record
    forwarder = self
    a_record = forwarder.build_a_record(domain_id: forwarder.domain.id, name: forwarder.name, content: FORWARDERIP, ttl: 60)
    a_record.save
  end

  def destroy_a_record
    forwarder = self
    forwarder.a_record.destroy if forwarder.a_record
  end

end
