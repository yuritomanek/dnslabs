class User < ActiveRecord::Base

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable, :timeoutable

  has_many :domains
  has_one :account

  def record_count
    count = 0
    domains.map { |domain| count += domain.records.count  }
    return count
  end

  def generate_api_token
    user = self

    token = loop do
      token = SecureRandom.urlsafe_base64(32)
      break token unless User.exists?(api_token: token)
    end

    user.api_token = token
    user.save
    return token

  end
end
