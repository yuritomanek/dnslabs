class ProcessPaymentWorker
  include Sidekiq::Worker

  def perform(user_id)
    sleep(2)
    user = User.find(user_id)
    if user.account.subscription_id.blank?
      user.account.create_subscription(Domain.enabled_count(user.id))
    else
      user.account.update_subscription(Domain.enabled_count(user.id))
    end
  end

end
