class WhoisWorker
  include Sidekiq::Worker

  def perform
    @whois = WhoisRecord.where("check_on IS NULL OR check_on < ?", Time.now - 1.week)
    @whois.each do |whois|
      CheckWhoisWorker.perform_async(whois.domain_id)
    end

    WhoisWorker.perform_at(12.hours.from_now)
  end

end
