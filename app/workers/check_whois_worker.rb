class CheckWhoisWorker
  include Sidekiq::Worker

  def perform(domain_id)
    @domain = Domain.find(domain_id)
    @domain.check_whois
  end

end
