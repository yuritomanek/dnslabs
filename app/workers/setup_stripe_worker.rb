class SetupStripeWorker
  include Sidekiq::Worker

  def perform(user_id)
    sleep(2)
    user = User.find(user_id)
    account = user.account
    account.create_customer
  end

end
