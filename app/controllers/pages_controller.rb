class PagesController < ApplicationController

  skip_before_filter :check_account

  def index
    redirect_to domains_path if current_user
    request.session_options[:skip] = true
    expires_in 10.minute, public: true
  end

end
