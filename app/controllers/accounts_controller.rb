class AccountsController < ApplicationController

  before_filter :check_user
  skip_before_filter :check_account

  def index
    @account = current_user.account
  end

  def process_payment
    begin
      account = current_user.account || current_user.create_account(plan: "domain")
      if account.create_customer(params[:stripeToken])
        redirect_to domains_path
      else
        render :payment
      end
    rescue => e
      puts e
      account.destroy if account
      render :payment
    end
  end

  def upgrade
    @account = current_user.account
  end

  def process_upgrade
    account = current_user.account
    if account.update_attributes(account_params) && account.process_upgrade
      redirect_to domains_path
    else
      render :upgrade
    end
  end

  private

    def check_user
      unless current_user
        redirect_to root_path
        return
      end
    end

    def account_params
      params.require(:account).permit(:id, :plan)
    end

end
