class DomainsController < ApplicationController

  before_filter :check_user

  def index
    @domains = current_user.domains.order("name ASC")
  end

  def new
    @domain = Domain.new
  end

  def create
    @domain = current_user.domains.new(domain_params)

    @domain.type = "NATIVE"
    @domain.primary_ns = PRIMARY_NS
    @domain.contact = CONTACT
    @domain.refresh = REFRESH
    @domain.retry = RETRY
    @domain.expire = EXPIRE
    @domain.minimum = MINIMUM

    if @domain.save
      redirect_to "/domains/"
    else
      render :new
    end
  end

  def edit
    @domain = current_user.domains.where(name: params[:domain]).first
    @records = @domain.records.order("type ASC, name ASC") if @domain
    @forwarders = @domain.forwarders.order("name ASC") if @domain
  end

  def delete
    @domain = current_user.domains.where(name: params[:domain]).first
    if @domain && @domain.destroy
      redirect_to domains_path
    else
      redirect_to domains_path
    end
  end

  def disable
    @domain = current_user.domains.where(name: params[:domain]).first
    @domain.disable
    redirect_to domains_path
  end

  def enable
    @domain = current_user.domains.where(name: params[:domain]).first
    @domain.enable
    redirect_to domains_path
  end

  private

    def check_user
      unless current_user
        redirect_to root_path
        return
      end
    end

    def domain_params
      params.require(:domain).permit(:id, :name)
    end

end
