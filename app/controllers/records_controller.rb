class RecordsController < ApplicationController

  before_filter :check_user

  def a
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.a_records.new
    render :new
  end

  def aaaa
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.aaaa_records.new
    render :new
  end

  def cname
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.cname_records.new
    render :new
  end

  def forwarder
    @domain = current_user.domains.where(name: params[:domain]).first
    @forwarder = @domain.forwarders.new
  end

  def create_forwarder
    @domain = current_user.domains.where(name: params[:domain]).first
    @forwarder = @domain.forwarders.new(forwarder_params)
    if @forwarder.save
      redirect_to "/domains/#{@domain.name}"
    else
      render :forwarder
    end
  end

  def delete_forwarder
    @domain = current_user.domains.where(name: params[:domain]).first
    @forwarder = @domain.forwarders.find(params[:id])
    if @forwarder.destroy
      redirect_to "/domains/#{@domain.name}"
    else
      redirect_to "/domains/#{@domain.name}"
    end
  end

  def mx
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.mx_records.new
    render :new
  end

  def spf
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.spf_records.new
    render :new
  end

  def srv
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.srv_records.new
    render :new
  end

  def txt
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.txt_records.new
    render :new
  end

  def create
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.records.new(record_params)
    if @record.save
      redirect_to "/domains/#{@domain.name}"
    else
      render :new
    end
  end

  def edit
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.records.find(params[:id]) if @domain
    if @domain.blank? || @record.blank? || !@record.allowed?
      flash[:error] = "You are not allowed to edit that record."
      redirect_to "/"
      return
    end
  end

  def update
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.records.find(params[:id]) if @domain
    if @domain.blank? || @record.blank? || !@record.allowed?
      redirect_to "/"
      return
    end
    if @record.update_attributes(record_params)
      redirect_to "/domains/#{@domain.name}"
    else
      render :edit
    end
  end

  def delete
    @domain = current_user.domains.where(name: params[:domain]).first
    @record = @domain.records.find(params[:id])
    if @record && @record.destroy
      redirect_to "/domains/#{@domain.name}"
    else
      redirect_to "/domains/#{@domain.name}"
    end
  end

  private

    def check_user
      unless current_user
        redirect_to root_path
        return
      end
    end

    def record_params
      params.require(:record).permit(:id, :name, :type, :content, :ttl, :prio)
    end

    def forwarder_params
      params.require(:forwarder).permit(:id, :name, :content)
    end

end
