class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :check_account

  private

    def check_account
      if current_user && current_user.account.blank?
        current_user.create_account(plan: "domain")
        SetupStripeWorker.perform_async(current_user.id)
      end
    end

end
