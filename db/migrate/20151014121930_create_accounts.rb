class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer :user_id
      t.string :plan
      t.boolean :trial , default: true
      t.string :customer_id
      t.string :subscription_id

      t.timestamps null: false
    end
  end
end
