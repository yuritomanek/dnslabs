class AddTtlToDomains < ActiveRecord::Migration
  def change
    add_column :domains, :ttl, :integer, default: 86400
  end
end
