class CreateForwarders < ActiveRecord::Migration
  def change
    create_table :forwarders do |t|
      t.integer :domain_id
      t.string :name
      t.string :content

      t.timestamps null: false
    end
  end
end
