class CreateWhoisRecords < ActiveRecord::Migration
  def change
    create_table :whois_records do |t|
      t.integer :domain_id
      t.string :status
      t.boolean :available, default: false
      t.boolean :registered, default: false
      t.datetime :created_on
      t.datetime :updated_on
      t.datetime :expires_on
      t.datetime :check_on

      t.timestamps null: false
    end
  end
end
