class AddExtraFields < ActiveRecord::Migration
  def change
    add_column :domains, :created_at, :datetime
    add_column :domains, :updated_at, :datetime
    add_column :records, :created_at, :datetime
    add_column :records, :updated_at, :datetime
    add_column :domains, :records_count, :integer, default: 0
  end
end
