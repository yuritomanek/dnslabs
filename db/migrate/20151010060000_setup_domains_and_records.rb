class SetupDomainsAndRecords < ActiveRecord::Migration
  def change

    create_table "comments", force: :cascade do |t|
      t.integer "domain_id",   limit: 4,     null: false
      t.string  "name",        limit: 255,   null: false
      t.string  "type",        limit: 10,    null: false
      t.integer "modified_at", limit: 4,     null: false
      t.string  "account",     limit: 40,    null: false
      t.text  "comment",     limit: 64000, null: false
    end

    add_index "comments", ["domain_id", "modified_at"], name: "comments_order_idx", using: :btree
    add_index "comments", ["domain_id"], name: "comments_domain_id_idx", using: :btree
    add_index "comments", ["name", "type"], name: "comments_name_type_idx", using: :btree

    create_table "cryptokeys", force: :cascade do |t|
      t.integer "domain_id", limit: 4,     null: false
      t.integer "flags",     limit: 4,     null: false
      t.boolean "active",    limit: 1
      t.text    "content",   limit: 65535
    end

    add_index "cryptokeys", ["domain_id"], name: "domainidindex", using: :btree

    create_table "domainmetadata", force: :cascade do |t|
      t.integer "domain_id", limit: 4,     null: false
      t.string  "kind",      limit: 32
      t.text    "content",   limit: 65535
    end

    add_index "domainmetadata", ["domain_id", "kind"], name: "domainmetadata_idx", using: :btree

    create_table "domains", force: :cascade do |t|
      t.string   "name",            limit: 255, null: false
      t.string   "master",          limit: 128
      t.integer  "last_check",      limit: 4
      t.string   "type",            limit: 6,   null: false
      t.integer  "notified_serial", limit: 4
      t.string   "account",         limit: 40
    end

    add_index "domains", ["name"], name: "name_index", unique: true, using: :btree

    create_table "records", force: :cascade do |t|
      t.integer  "domain_id",    limit: 4
      t.string   "name",         limit: 255
      t.string   "type",         limit: 10
      t.text     "content",      limit: 64000
      t.integer  "ttl",          limit: 4
      t.integer  "prio",         limit: 4
      t.integer  "change_date",  limit: 4
      t.boolean  "disabled",     limit: 1,     default: false
      t.string   "ordername",    limit: 255
      t.boolean  "auth",         limit: 1,     default: true
    end

    add_index "records", ["domain_id", "ordername"], name: "recordorder", using: :btree
    add_index "records", ["domain_id"], name: "domain_id", using: :btree
    add_index "records", ["name", "type"], name: "nametype_index", using: :btree

    create_table "supermasters", id: false, force: :cascade do |t|
      t.string "ip",         limit: 64,  null: false
      t.string "nameserver", limit: 255, null: false
      t.string "account",    limit: 40,  null: false
    end

    create_table "tsigkeys", force: :cascade do |t|
      t.string "name",      limit: 255
      t.string "algorithm", limit: 50
      t.string "secret",    limit: 255
    end

    add_index "tsigkeys", ["name", "algorithm"], name: "namealgoindex", unique: true, using: :btree

  end
end
