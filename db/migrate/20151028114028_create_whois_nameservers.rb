class CreateWhoisNameservers < ActiveRecord::Migration
  def change
    create_table :whois_nameservers do |t|
      t.integer :whois_record_id
      t.string :name

      t.timestamps null: false
    end
  end
end
