# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151114224028) do

  create_table "accounts", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.string   "plan",            limit: 255
    t.boolean  "trial",                       default: true
    t.string   "customer_id",     limit: 255
    t.string   "subscription_id", limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer "domain_id",   limit: 4,     null: false
    t.string  "name",        limit: 255,   null: false
    t.string  "type",        limit: 10,    null: false
    t.integer "modified_at", limit: 4,     null: false
    t.string  "account",     limit: 40,    null: false
    t.text    "comment",     limit: 65535, null: false
  end

  add_index "comments", ["domain_id", "modified_at"], name: "comments_order_idx", using: :btree
  add_index "comments", ["domain_id"], name: "comments_domain_id_idx", using: :btree
  add_index "comments", ["name", "type"], name: "comments_name_type_idx", using: :btree

  create_table "cryptokeys", force: :cascade do |t|
    t.integer "domain_id", limit: 4,     null: false
    t.integer "flags",     limit: 4,     null: false
    t.boolean "active"
    t.text    "content",   limit: 65535
  end

  add_index "cryptokeys", ["domain_id"], name: "domainidindex", using: :btree

  create_table "domainmetadata", force: :cascade do |t|
    t.integer "domain_id", limit: 4,     null: false
    t.string  "kind",      limit: 32
    t.text    "content",   limit: 65535
  end

  add_index "domainmetadata", ["domain_id", "kind"], name: "domainmetadata_idx", using: :btree

  create_table "domains", force: :cascade do |t|
    t.string   "name",            limit: 255,                 null: false
    t.string   "master",          limit: 128
    t.integer  "last_check",      limit: 4
    t.string   "type",            limit: 6,                   null: false
    t.integer  "notified_serial", limit: 4
    t.string   "account",         limit: 40
    t.integer  "user_id",         limit: 4
    t.integer  "ttl",             limit: 4,   default: 86400
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "records_count",   limit: 4,   default: 0
  end

  add_index "domains", ["name"], name: "name_index", unique: true, using: :btree

  create_table "forwarders", force: :cascade do |t|
    t.integer  "domain_id",  limit: 4
    t.string   "name",       limit: 255
    t.string   "content",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "records", force: :cascade do |t|
    t.integer  "domain_id",    limit: 4
    t.string   "name",         limit: 255
    t.string   "type",         limit: 10
    t.text     "content",      limit: 65535
    t.integer  "ttl",          limit: 4
    t.integer  "prio",         limit: 4
    t.integer  "change_date",  limit: 4
    t.boolean  "disabled",                   default: false
    t.string   "ordername",    limit: 255
    t.boolean  "auth",                       default: true
    t.integer  "forwarder_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "records", ["domain_id", "ordername"], name: "recordorder", using: :btree
  add_index "records", ["domain_id"], name: "domain_id", using: :btree
  add_index "records", ["name", "type"], name: "nametype_index", using: :btree

  create_table "supermasters", id: false, force: :cascade do |t|
    t.string "ip",         limit: 64,  null: false
    t.string "nameserver", limit: 255, null: false
    t.string "account",    limit: 40,  null: false
  end

  create_table "tsigkeys", force: :cascade do |t|
    t.string "name",      limit: 255
    t.string "algorithm", limit: 50
    t.string "secret",    limit: 255
  end

  add_index "tsigkeys", ["name", "algorithm"], name: "namealgoindex", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.integer  "failed_attempts",        limit: 4,   default: 0,  null: false
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "api_token",              limit: 255
  end

  add_index "users", ["api_token"], name: "index_users_on_api_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "whois_nameservers", force: :cascade do |t|
    t.integer  "whois_record_id", limit: 4
    t.string   "name",            limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "whois_records", force: :cascade do |t|
    t.integer  "domain_id",  limit: 4
    t.string   "status",     limit: 255
    t.boolean  "available",              default: false
    t.boolean  "registered",             default: false
    t.datetime "created_on"
    t.datetime "updated_on"
    t.datetime "expires_on"
    t.datetime "check_on"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

end
