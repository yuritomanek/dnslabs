require File.expand_path('../boot', __FILE__)

require 'rails/all'
Bundler.require(*Rails.groups)

module Dnslabs
  class Application < Rails::Application
    config.active_record.raise_in_transactional_callbacks = true
    config.sass.preferred_syntax = :sass
    config.generators.stylesheets = false
    config.generators.javascripts = false

    config.autoload_paths += %W["#{config.root}/app/validators/"]

    config.before_configuration do
      env_file = File.join(Rails.root, 'config', 'local_env.yml')
      YAML.load(File.open(env_file)).each do |key, value|
        ENV[key.to_s] = value
      end if File.exists?(env_file)
    end

  end
end
