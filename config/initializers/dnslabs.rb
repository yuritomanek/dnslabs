RECORD_TYPES = ["A","AAAA","CNAME","MX","SPF","SRV","TXT"]
TTL = [["1 Minute",60],["10 minutes",600],["1 hour",3600],["2 hours",7200],["4 hours",14400],["8 hours",28800],["12 hours",43200],["1 day",86400],["3 days",259200]]
PUBLIC_KEY = ENV['PUBLIC_KEY']
SECRET_KEY = ENV['SECRET_KEY']

BASIC_PLAN = 5
PLUS_PLAN = 25

FORWARDERIP = "104.197.90.119"

PRIMARY_NS = "ns1.dnslabs.co"
CONTACT = "hostmaster@dnslabs.co"
REFRESH = 10800
RETRY = 3600
EXPIRE = 2419200
MINIMUM  = 3600
