# Change these
server '104.197.90.119', port: 22, roles: [:web, :app, :db], primary: true

set :repo_url,        'git@bitbucket.org:yuritomanek/dnslabs.git'
set :application,     'dnslabs'
set :user,            'yuri'

set :rbenv_type, :user
set :rbenv_ruby, '2.2.3'

# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/opt/www/#{fetch(:application)}"
set :ssh_options,     { forward_agent: true, user: fetch(:user) }

set :sidekiq_default_hooks, true
set :sidekiq_pid, File.join(shared_path, 'tmp', 'pids', 'sidekiq.pid')
set :sidekiq_env, fetch(:rack_env, fetch(:rails_env, fetch(:stage)))
set :sidekiq_log, File.join(shared_path, 'log', 'sidekiq.log')
set :sidekiq_options, nil
set :sidekiq_require, nil
set :sidekiq_tag, nil
set :sidekiq_config, File.join(current_path, 'config', 'sidekiq.yml')
set :sidekiq_queue, nil
set :sidekiq_timeout, 10
set :sidekiq_role, :app
set :sidekiq_processes, 1
set :sidekiq_options_per_process, nil
set :sidekiq_concurrency, nil
set :sidekiq_monit_templates_path, 'config/deploy/templates'
set :sidekiq_monit_use_sudo, true
set :sidekiq_cmd, "#{fetch(:bundle_cmd, "bundle")} exec sidekiq" # Only for capistrano2.5
set :sidekiqctl_cmd, "#{fetch(:bundle_cmd, "bundle")} exec sidekiqctl" # Only for capistrano2.5
set :sidekiq_user, nil #user to run sidekiq as

## Defaults:
# set :scm,           :git
# set :branch,        :master
# set :format,        :pretty
# set :log_level,     :debug
# set :keep_releases, 5

## Linked Files & Directories (Default None):
set :linked_files, %w{config/database.yml config/secrets.yml config/local_env.yml}
set :linked_dirs,  %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'passenger:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'deploy:restart'
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma
