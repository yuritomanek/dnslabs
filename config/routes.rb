require 'sidekiq/web'

Rails.application.routes.draw do

  mount Sidekiq::Web => '/queue'

  devise_for :users

  root 'pages#index'

  get '/privacy-policy' => 'pages#privacy'
  get '/terms' => 'pages#terms'

  get '/ping', to: proc { [200, {}, ['pong']] }

  get '/account' => 'accounts#index', as: 'account'

  get '/payment' => 'accounts#payment', as: 'payment'
  post '/payment/process' => 'accounts#process_payment', as: 'process_payment'
  get '/payment/upgrade' => 'accounts#upgrade', as: 'upgrade'
  patch '/payment/upgrade' => 'accounts#process_upgrade'

  get '/domains' => 'domains#index', as: 'domains'
  post '/domains' => 'domains#create', as: 'domain'
  get '/domains/new' => 'domains#new', as: 'new_domain'
  get '/domains/:domain' => 'domains#edit', as: 'edit_domain', constraints: { domain: /[^\/]+/ }
  delete '/domains/:domain' => 'domains#delete', as: 'delete_domain', constraints: { domain: /[^\/]+/ }
  post '/domains/:domain/disable' => 'domains#disable', as: 'disable_domain', constraints: { domain: /[^\/]+/ }
  post '/domains/:domain/enable' => 'domains#enable', as: 'enable_domain', constraints: { domain: /[^\/]+/ }

  get '/domains/:domain/records/new/a' => 'records#a', as: 'new_a_record', constraints: { domain: /[^\/]+/ }
  get '/domains/:domain/records/new/aaaa' => 'records#aaaa', as: 'new_aaaa_record', constraints: { domain: /[^\/]+/ }
  get '/domains/:domain/records/new/cname' => 'records#cname', as: 'new_cname_record', constraints: { domain: /[^\/]+/ }
  get '/domains/:domain/records/new/forwarder' => 'records#forwarder', as: 'new_forwarder_record', constraints: { domain: /[^\/]+/ }
  post '/domains/:domain/records/new/forwarder' => 'records#create_forwarder', as: 'delete_forwarder_record', constraints: { domain: /[^\/]+/ }
  delete '/domains/:domain/records/forwarder/:id' => 'records#delete_forwarder', constraints: { domain: /[^\/]+/ }
  get '/domains/:domain/records/new/mx' => 'records#mx', as: 'new_mx_record', constraints: { domain: /[^\/]+/ }
  get '/domains/:domain/records/new/spf' => 'records#spf', as: 'new_spf_record', constraints: { domain: /[^\/]+/ }
  get '/domains/:domain/records/new/srv' => 'records#srv', as: 'new_srv_record', constraints: { domain: /[^\/]+/ }
  get '/domains/:domain/records/new/txt' => 'records#txt', as: 'new_txt_record', constraints: { domain: /[^\/]+/ }
  post '/domains/:domain/records' => 'records#create', as: 'record', constraints: { domain: /[^\/]+/ }
  get '/domains/:domain/records/:id' => 'records#edit', as: 'edit_record', constraints: { domain: /[^\/]+/ }
  post '/domains/:domain/records/:id' => 'records#update', constraints: { domain: /[^\/]+/ }
  delete '/domains/:domain/records/:id' => 'records#delete', as: 'delete_record', constraints: { domain: /[^\/]+/ }

end
